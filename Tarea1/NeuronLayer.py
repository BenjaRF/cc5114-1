import Neuron
import random
import numpy as np

'''
cosas pendientes en Layer:
- crear una layer dummy, algo asi como null pattern, ya que tengo que usar esa en caso de previous_layer = None
y tanbm usarla para el principio de next_layer 
'''

def sigmoid(x):
    return 1.0/(1.0 + np.exp(-x))


class NeuronLayer:
    def __init__(self, neurons_number, inputs_number = None, previous_layer = None, function = None, learn_rate = None):
        if function is None:
            function = sigmoid
        if inputs_number is None:
            if previous_layer is None:
                raise("previous layer and number of inputs not defined, need more information")
            else:
                inputs_number = previous_layer.getNeuronNumber()
        if learn_rate is None:
            learn_rate = 0.5
        self.neurons = []
        self.neuron_number = neurons_number
        self.learning_rate = learn_rate
        self.prev_layer = previous_layer
        self.next_layer = None
        self.input_number = inputs_number
        self.last_output = []


        for i in range(self.neuron_number):
            aux_weights = []
            for i in range(self.input_number):
                aux_weights.append(random.uniform(-1, 1))
            self.neurons.append(Neuron.Neuron(aux_weights, random.uniform(0, 1), function))

    def setNextLayer(self, a_layer):
        self.next_layer = a_layer
        return

    def setLeaningRate(self, new_rate):
        self.learning_rate = new_rate
        return

    def getNeurons(self):
        return self.neurons

    def getNeuronNumber(self):
        return self.neuron_number

    def getLastOutput(self):
        return self.last_output

    def feed(self, inputs_list):
        self.last_output = []
        for i in range(self.neuron_number):
            self.last_output.append(self.neurons[i].getOutput(inputs_list))
        if not(self.next_layer is None):
            self.next_layer.feed(self.last_output)
        return

    def backPropagate(self, desired_output = None):
        if(desired_output is None):
            desired_output = [0]

        #last layer case:
        if self.next_layer is None:
            for i in range(self.neuron_number):
                error = desired_output[i] - self.last_output[i]
                self.neurons[i].adjustDelta(error)

        #hidden layer case (not using the desired_output):
        else:
            for i in range(self.neuron_number):
                error = 0.0
                next_layer_neurons = self.next_layer.getNeurons()
                for n in next_layer_neurons:
                    error += n.getWeight(i) * n.getDelta()
                self.neurons[i].adjustDelta(error)

        if not(self.prev_layer is None):
            self.prev_layer.backPropagate()
        return

    def updateWeight(self, input_list):
        #if all neurons has delta != Null
        '''
        no se si necesito eso de arriba

        segun el codigo del profe aqui se setea el learning rate como 0.5
        ademas en ese codigo se trata de forma distinta a la primera capa ya que el input es el inicial
        en cambio la idea mia es entregar el output de una forma similar a estos input , i.e. tratarlos de
        la misma manera.
        '''
        for n in self.neurons:
            n.adjustWeight(self.learning_rate, input_list)
            n.adjustBias(self.learning_rate)

        if not(self.next_layer is None):
            self.next_layer.updateWeight(self.last_output)
        return



