
'''
1.- learning rate
2.- topologia: -number of hidden layers
               -number of neurons per layer
3.- number of outputs
4.- dataset
'''

def transferDerivative(x):
    return x * (1.0 - x)

class Neuron:
    def __init__(self, weights_list, bias, output_fun):
        self.weights = weights_list
        self.weights_number = len(weights_list)
        self.bias = bias
        self.last_output = 0.0
        self.last_delta = 0.0
        self.output_function = output_fun

    def setWeight(self, new_weights):
        self.weights = new_weights
        return

    def setBias(self, new_bias):
        self.bias = new_bias
        return

    def setOutputFun(self, new_fun):
        self.output_function = new_fun

    def getWeights(self):
        return self.weights

    def getWeight(self, index):
        if (index < self.weights_number):
            return self.weights[index]
        return

    def adjustWeight(self, learn_rate, input_list):
        for i in range(self.weights_number):
            self.weights[i] += (learn_rate * self.last_delta * input_list[i])
        return

    def adjustBias(self, learn_rate):
        self.bias += (learn_rate * self.last_delta)
        return

    def getDelta(self):
        return self.last_delta

    def adjustDelta(self , error):
        self.last_delta = error * transferDerivative(self.last_output)
        return

    def getOutput(self, input_list):
        # return sigmoidFun(x1 * self.w1 + x2 * self.w2 + self.b) > 0.5
        value = self.bias
        # Falta un try catch, en caso de que el largo de los input sea distinto del weights
        for i in range(self.weights_number):
            value = value + (self.weights[i] *  input_list[i])
        self.last_output = self.output_function(value)
        return self.last_output
