import unittest as ut
import NeuralNetwork
import NeuronLayer
import Neuron
import random
import numpy as np
import math
import matplotlib.pyplot as plt

def sigmoid(x):
    return 1.0/(1.0 + np.exp(-x))

'''
The format of the outputs must be a list of values between 0 and 1. 
Returns a tuple with (true_positive_rate, false_positive_rate, mean_abs_error, mean_sqr_error)
'''
def performance(desired_outputs, outputs):

    good_guess = 0
    attempts = len(outputs)
    for i in range(attempts):
        if desired_outputs[i].index(np.amax(desired_outputs[i])) == outputs[i].index(np.amax(outputs[i])):
            good_guess += 1

    precision = good_guess/attempts

    #mean absolute error:
    mean_abs_error = 0.0
    for i in range(attempts):
        #considering outputs with more than one value
        error = 0.0
        for j in range(len(outputs[0])):
            error += abs(desired_outputs[i][j] -  outputs[i][j])
        mean_abs_error += error/len(outputs[0])
    mean_abs_error = mean_abs_error/attempts
    return [precision, mean_abs_error]

'''
Plotting process of the Neural network performance
'''
def plottingPerformance(epoch_number, precision_array, mean_abs_error_array):

    plt.figure()
    plt.subplot(121)
    plt.xlabel('Epoch iterations')
    plt.ylabel('Precision measure')
    plt.title('Precision')
    #plt.ylim(0.0, 1.0)
    plt.plot(list(range(epoch_number)), precision_array, 'k')

    plt.subplot(122)
    plt.xlabel('Epoch iterations')
    plt.ylabel('Mean Absolute Error')
    plt.title('Error')
    plt.ylim(0.0, 1.0)
    plt.plot(list(range(epoch_number)), mean_abs_error_array, 'k')
    plt.show()
    return

'''
extract the element in the index position of the sub-arrays in "an_array"
'''
def extractData(an_array, index):
    aux = []
    for i in range(len(an_array)):
        aux.append(an_array[i][index])
    return aux

'''
function used to normalice inputs according to the range of his values
'''
def normalice(x, inmax, inmin, outmax, outmin):
    return ((x - inmin)*(outmax - outmin)/(inmax - inmin)) - outmin

def main():
    print("IRIS NETWORK")
    f = open("iris.data", 'r')
    data = []
    for line in f:
        #extract the data of the file and put it on a array
        data.append(np.array(line[:-1].split(',')))
    random.shuffle(data)

    inputs = []
    expected_outputs = []

    for a_input in data:
        #a_input format example: ['5.2','3.5','1.5','0.2','Iris-setosa']
        #stores the input value of the data in the "inputs" array as float.
        inputs.append(a_input[:-1].astype(np.float))
        #stores the desired output asociated with the input value codified
        iris = a_input[-1]
        if iris == 'Iris-setosa':
            expected_outputs.append([1, 0, 0])
        elif iris == 'Iris-versicolor':
            expected_outputs.append([0, 1, 0])
        else:
            expected_outputs.append([0, 0, 1])



    # NORMALIZATION of input
    max_input = [7.9, 4.4, 6.9, 2.5]
    min_input = [4.3, 2.0, 1.0, 0.1]
    for a_input in inputs:
        for i in range(len(a_input)):
            a_input[i] = normalice(a_input[i],max_input[i], min_input[i], 1.0, 0.0)

    training_inputs = inputs[:int(len(inputs)/2)]
    testing_inputs = inputs[int(len(inputs)/2):]
    training_expected_outputs = expected_outputs[:int(len(expected_outputs)/2)]
    testing_expected_outputs = expected_outputs[int(len(expected_outputs)/2):]

    #Inicial values
    input_number = 4
    neuron_per_layer = 8
    hidden_layer_number = 3
    learn_rate = 1.0
    epoch_iterations = 1500

    # CREATING NETWORK
    network = NeuralNetwork.NeuralNetwork(input_number, learn_rate)
    for i in range(hidden_layer_number):
        network.addLayer(neuron_number=neuron_per_layer)
    network.addLayer(neuron_number=3)

    # TRAINING
    performance_array = []

    for i in range(epoch_iterations):
        # one epoch cycle
        for j in range(len(training_inputs)):
            network.train(training_inputs[j], training_expected_outputs[j])
        # test and record PERFORMANCE per epoch trained
        outputs = []
        for k in range(len(testing_inputs)):
            outputs.append(network.feed(testing_inputs[k]))
        performance_array.append(performance(testing_expected_outputs, outputs))

    precision = extractData(performance_array, 0)
    mean_abs_error= extractData(performance_array, 1)
    plottingPerformance(epoch_iterations, precision, mean_abs_error)

    '''
    #OUTPUT RESULT
    outputs = []
    for k in range(len(testing_inputs)):
        outputs.append(network.feed(testing_inputs[k]))

    for i in range(len(testing_inputs)):
        print("output: ",outputs[i], " expected output: ", testing_expected_outputs[i])
    '''

    return

if __name__ == '__main__':
    main()