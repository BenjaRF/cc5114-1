'''
1.- learning rate
2.- topologia: -number of hidden layers
               -number of neurons per layer
3.- number of outputs
4.- dataset
'''

import Neuron
import NeuronLayer


class NeuralNetwork:
    def __init__(self, input_number, learn_rate):
        self.layers = []
        self.input_number = input_number
        self.learn_rate = learn_rate
        self.inputs = []
        self.outputs = []
        self.first_layer = None
        self.last_layer = None
        self.layer_number = 0

    def addLayer(self, neuron_number, output_function = None):
        if(self.layer_number == 0):
            self.layers.append(NeuronLayer.NeuronLayer(neuron_number, inputs_number=self.input_number, function=output_function, learn_rate=self.learn_rate))
            self.first_layer = self.layers[0]
        else:
            self.layers.append(NeuronLayer.NeuronLayer(neuron_number, previous_layer = self.layers[self.layer_number - 1], function=output_function, learn_rate=self.learn_rate))
            #only in the next line, the layer_number is not updated, so the last_layer has the layer_number as index
            self.layers[self.layer_number - 1].setNextLayer(self.layers[self.layer_number])

        self.layer_number += 1 #Now it is updated
        self.last_layer = self.layers[self.layer_number - 1]
        return

    def setAllLearningRate(self, new_rate):
        self.learn_rate = new_rate
        for i in range(self.layer_number):
            self.layers[i].setLearningRate(self.learn_rate)
        return

    #get the output asociated with the input list
    def feed(self, input_list):
        self.first_layer.feed(input_list)
        return self.last_layer.getLastOutput()

    #train the NN using a list of inputs and desired outputs
    def train(self, inputs_lists, desired_output):
        output = self.feed(inputs_lists)
        self.backPropagateError(desired_output)
        self.updateWeight(inputs_lists)
        return

    def backPropagateError(self, desired_output):
        self.last_layer.backPropagate(desired_output)
        return

    def updateWeight(self, input_list):
        self.first_layer.updateWeight(input_list)
        return


