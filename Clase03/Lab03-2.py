# -*- coding: utf-8 -*-
"""
Created on Wed Aug  9 17:00:14 2017

@author: benro
"""
#mejorar, hacerlo con cantidad de weights variable, asi mismo la cantidad
#de inputs debe ser variable, pero antes hacer un test o un chequeo de que
#la cantiada de W's e inputs sea la misma o levantar un error.

import numpy as np
import matplotlib.pyplot as plt
import random


def sigmoidFun(x):
    return 1.0/(1.0 + np.exp(-x))

class Perceptron:
    def __init__(self, weight1, weight2, bias, tasa_de_aprendizaje):
        self.w1 = weight1
        self.w2 = weight2
        self.b = bias
        self.c = tasa_de_aprendizaje
        
    def setWeight1(self, w):
        self.w1 = w
        return
        
    def setWeight2(self, w):
        self.w2 = w
        return
    
    def setBias(self, bias):
        self.b = bias
        return

    def setLearning(self,newC):
        self.c = newC
        return
    
    def getWeight1(self):
        return self.w1

    def getWeight2(self):
        return self.w2
    
    def getOutput(self, x1, x2):
        return sigmoidFun(x1 * self.w1 + x2 * self.w2 + self.b)>0.5
    
    def learn(self, x1, x2, resp):
        precep_resp = self.getOutput(x1,x2)
        if (precep_resp and (resp == False)): # if(True and not(False))...
            self.setWeight1(self.w1 - (self.c * x1))
            self.setWeight2(self.w2 - (self.c * x2))
            return False
        elif ((precep_resp == False) and resp): # if(not(False) and True)...
            self.setWeight1(self.w1 + (self.c * x1))
            self.setWeight2(self.w2 + (self.c * x2))
            return False
        return True
    
    
    
def main():
    c = 0.1
    bias = 0.5
    w1 = 1
    w2 = 1
    my_perc = Perceptron(w1, w2, bias, c)
    aciertos = 0
    desemp = []
    w = []

    #TRAINING
    for i in range(5000):
        x = random.randint(-100,100)
        y = random.randint(-100,100)
        resp = True
        if ((-2*x) + 3 >= y):
            resp = False
        if (my_perc.learn(x, y, resp)):
            aciertos+=1
        desemp.append(aciertos/(i+1))
        w.append(my_perc.getWeight1())
        
    ans_true_listX = []
    ans_true_listY = []
    ans_false_listX = []
    ans_false_listY = []
    
    for i in range(1000):
        x = random.randint(-100,100)
        y = random.randint(-100,100)
        if(my_perc.getOutput(x,y)):
            ans_true_listX.append(x)
            ans_true_listY.append(y)
        else:
            ans_false_listX.append(x)
            ans_false_listY.append(y)

    
    #PLOTING RESULTS
    plt.figure(1)
    plt.subplot(121)
    plt.ylabel('Success during Training')
    plt.xlabel('Training Iterations')
    plt.title('Proportion of Success')
    plt.plot(list(range(len(desemp[10:]))),desemp[10:],'k')
    plt.subplot(122)
    plt.title('Clasification of Points by Trained Perceptron ')
    plt.plot(ans_true_listX, ans_true_listY, "bo",ans_false_listX, ans_false_listY, "ro",[-100, 100], [(-2*-100)+3, (-2*100)+3], 'k')
    plt.show()
    
    plt.figure()
    plt.subplot(111)
    plt.ylabel('weight value')
    plt.xlabel('Training Iterations')
    plt.title('weight variation')
    plt.plot(list(range(len(w))),w,'k')
    plt.show()
    

if __name__ == '__main__':
    main()