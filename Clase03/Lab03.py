# -*- coding: utf-8 -*-
"""
Created on Wed Aug  9 17:00:14 2017

@author: benro
"""

import unittest as ut
import numpy as np

def sigmoidFun(x):
    return 1.0/(1.0 + np.exp(-x))

class Perceptron:
    def __init__(self, weight1, weight2, bias):
        self.w1 = weight1
        self.w2 = weight2
        self.b = bias
        
    def setWeight1(self, w):
        self.w1 = w
        return
        
    def setWeight2(self, w):
        self.w2 = w
        return
    
    def setBias(self, bias):
        self.b = bias
        return
        
    def getOutput(self, x1, x2):
        
        return sigmoidFun(x1 * self.w1 + x2 * self.w2 + self.b)>0.5
    
    
class TestPerceptron(ut.TestCase):
    
    def testAND(self):
        AND = Perceptron(1,1,-1)
        self.assertEqual(AND.getOutput(0,0), 0)
        self.assertEqual(AND.getOutput(0,1), 0)
        self.assertEqual(AND.getOutput(1,0), 0)
        self.assertEqual(AND.getOutput(1,1), 1)
    
    def testOR(self):
        OR = Perceptron(1,1,0)
        self.assertEqual(OR.getOutput(0,0), 0)
        self.assertEqual(OR.getOutput(0,1), 1)
        self.assertEqual(OR.getOutput(1,0), 1)
        self.assertEqual(OR.getOutput(1,1), 1)
        
    def testNAND(self):
        NAND = Perceptron(-2,-2,3)
        self.assertEqual(NAND.getOutput(0,0), 1)
        self.assertEqual(NAND.getOutput(0,1), 1)
        self.assertEqual(NAND.getOutput(1,0), 1)
        self.assertEqual(NAND.getOutput(1,1), 0)
    

if __name__ == '__main__':
    ut.main()