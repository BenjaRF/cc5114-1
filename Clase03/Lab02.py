# -*- coding: utf-8 -*-
"""
Created on Wed Aug  2 16:56:44 2017

@author: benro
"""
import matplotlib.pyplot as plt
import random

def test100cases(cell): 
    miss = 0
    for i in range(100):
        x = random.randint(-100, 100)
        y = random.randint(-100, 100)
        answer = True
        if ((-2*x) + 3 >= y):
            answer = False
        if(cell.getOutput(x,y) != answer):
            miss += 1
    return (100 - miss)/100

class Perceptron:
    def __init__(self, weight1, weight2, bias, tasa_de_aprendizaje):
        self.w1 = weight1
        self.w2 = weight2
        self.b = bias
        self.c = tasa_de_aprendizaje
        
    def setWeight1(self, w):
        self.w1 = w
        return
        
    def setWeight2(self, w):
        self.w2 = w
        return
    
    def setBias(self, bias):
        self.b = bias
        return

    def setLearning(self,newC):
        self.c = newC
        return
    
    def getWeight1(self):
        return self.w1

    def getWeight2(self):
        return self.w2

    def getOutput(self, x1, x2):
        return (x1 * self.w1 + x2 * self.w2 + self.b) > 0 
    
    def learn(self, x1, x2, resp):
        precep_resp = self.getOutput(x1,x2)
        if (precep_resp and (resp == False)): # if(True and not(False))...
            self.setWeight1(self.w1 - (self.c * x1))
            self.setWeight2(self.w2 - (self.c * x2))
            return False
        elif ((precep_resp == False) and resp): # if(not(False) and True)...
            self.setWeight1(self.w1 + (self.c * x1))
            self.setWeight2(self.w2 + (self.c * x2))
            return False
        return True

def main():
    c = 0.01
    bias = 0.5
    w1 = 1
    w2 = 1
    performance = []
    #TRAINING
    for i in range(500):
        my_perc = Perceptron(w1, w2, bias, c)
        for j in range(i):
            x = random.randint(-100,100)
            y = random.randint(-100,100)
            resp = True
            if ((-2*x) + 3 >= y):
                resp = False
            my_perc.learn(x, y, resp)
        
        performance.append(test100cases(my_perc))


    plt.figure()
    plt.subplot(121)
    plt.xlabel('Training iterations')
    plt.ylabel('Success testing 100 points')
    plt.title('Performance')
    plt.ylim(0.0, 1.0)
    plt.plot(list(range(len(performance))),performance,'k')
    
    
    #TESTING RESULTS
    ans_true_listX = []
    ans_true_listY = []
    ans_false_listX = []
    ans_false_listY = []
    
    for i in range(1000):
        x = random.randint(-100,100)
        y = random.randint(-100,100)
        if(my_perc.getOutput(x,y)):
            ans_true_listX.append(x)
            ans_true_listY.append(y)
        else:
            ans_false_listX.append(x)
            ans_false_listY.append(y)
            
    plt.subplot(122)
    plt.title('Clasification of Points by Trained Perceptron ')
    plt.plot(ans_true_listX, ans_true_listY, "bo",ans_false_listX, ans_false_listY, "ro",[-100, 100], [(-2*-100)+3, (-2*100)+3], 'k')
    plt.show()
    



if __name__ == '__main__':
    main()