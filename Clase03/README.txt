https://bitbucket.org/BenjaRF/cc5114-1/src

Implementations

Lab02 (update):
- Leaning in a perceptron, giving the points of a partition of the space, 
generates graphs with the process and result of the perceptron behavior.

Lab03:
- A perceptron, with the AND, OR, NAND behavior using the sigmoid function.
with test.

Lab03-sigmoid:
- Leaning in a perceptron, giving the points of a partition of the space,
using the sigmon function. Generates graphs with the process and result of
the perceptron behavior.