https://bitbucket.org/BenjaRF/cc5114-1/src

Implementations

Lab01:
- a Perceptron, with the AND, OR, NAND behavior
- the summing bit gate

Lab02:
- Leaning in a Perceptron, giving the points of a partition of the space.

