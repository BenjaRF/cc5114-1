# -*- coding: utf-8 -*-
"""
Created on Wed Aug  2 16:56:44 2017

@author: benro
"""
import unittest as ut

class Perceptron:
    def __init__(self, weight1, weight2, bias):
        self.w1 = weight1
        self.w2 = weight2
        self.b = bias
        
    def setWeight1(self, w):
        self.w1 = w
        return
        
    def setWeight2(self, w):
        self.w2 = w
        return
    
    def setBias(self, bias):
        self.b = bias
        return
        
    def getOutput(self, x1, x2):
        return (x1 * self.w1 + x2 * self.w2 + self.b) > 0 
    
class SummingNumberGate:
    def __init__(self):
        #se deben tener los perceptrons NAND guardados aca
        self.NAND = Perceptron(-2,-2,3)
    
    def getOutput(self, x1, x2):
        #layer1
        outNand1 = self.NAND.getOutput(x1,x2)
        #layer2
        outNand2 = self.NAND.getOutput(x1,outNand1)
        outNand3 = self.NAND.getOutput(outNand1,x2)
        outNand4 = self.NAND.getOutput(outNand1,outNand1) #carry bit: x1x2
        #layer3
        outNand5 = self.NAND.getOutput(outNand2,outNand3) #sum: x1 + x2
        return (outNand5, outNand4) #tupla de (sum, carryBit)
                                 
    
class TestPerceptron(ut.TestCase):
    
    def testAND(self):
        AND = Perceptron(1,1,-1)
        self.assertEqual(AND.getOutput(0,0), 0)
        self.assertEqual(AND.getOutput(0,1), 0)
        self.assertEqual(AND.getOutput(1,0), 0)
        self.assertEqual(AND.getOutput(1,1), 1)
    
    def testOR(self):
        OR = Perceptron(1,1,0)
        self.assertEqual(OR.getOutput(0,0), 0)
        self.assertEqual(OR.getOutput(0,1), 1)
        self.assertEqual(OR.getOutput(1,0), 1)
        self.assertEqual(OR.getOutput(1,1), 1)
        
    def testNAND(self):
        NAND = Perceptron(-2,-2,3)
        self.assertEqual(NAND.getOutput(0,0), 1)
        self.assertEqual(NAND.getOutput(0,1), 1)
        self.assertEqual(NAND.getOutput(1,0), 1)
        self.assertEqual(NAND.getOutput(1,1), 0)
        
    def testSummingNumberGate(self):
        SNG = SummingNumberGate()
        self.assertEqual(SNG.getOutput(1,1),(0,1))
        self.assertEqual(SNG.getOutput(1,0),(1,0))
        self.assertEqual(SNG.getOutput(0,1),(1,0))
        self.assertEqual(SNG.getOutput(0,0),(0,0))
    
    

if __name__ == '__main__':
    ut.main()