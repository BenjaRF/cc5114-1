# -*- coding: utf-8 -*-
"""
Created on Wed Aug  9 23:46:49 2017

@author: benro
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Aug  2 16:56:44 2017

@author: benro
"""
import matplotlib.pyplot as plt
import random

class Perceptron:
    def __init__(self, weight1, weight2, bias, tasa_de_aprendizaje):
        self.w1 = weight1
        self.w2 = weight2
        self.b = bias
        self.c = tasa_de_aprendizaje
        
    def setWeight1(self, w):
        self.w1 = w
        return
        
    def setWeight2(self, w):
        self.w2 = w
        return
    
    def setBias(self, bias):
        self.b = bias
        return

    def setLearning(self,newC):
        self.c = newC
        return
    
    def getWeight1(self):
        return self.w1

    def getWeight2(self):
        return self.w2

    def getOutput(self, x1, x2):
        return (x1 * self.w1 + x2 * self.w2 + self.b) > 0 
    
    def learn(self, x1, x2, resp):
        precep_resp = self.getOutput(x1,x2)
        if (precep_resp and not(resp)): # if(True and not(False))...
            self.setWeight1(self.w1 - (self.c * x1))
            self.setWeight2(self.w2 - (self.c * x2))
            return False
        elif (not(precep_resp) and resp): # if(not(False) and True)...
            self.setWeight1(self.w1 + (self.c * x1))
            self.setWeight2(self.w2 + (self.c * x2))
            return False
        return True

def main():
    c = 0.01
    bias = 0.5
    w1 = 1
    w2 = 1
    my_perc = Perceptron(w1, w2, bias, c)
    for i in range(10000):
        x = random.randint(-100,100)
        y = random.randint(-100,100)
        resp = True
        if ((-2*x) + 3 >= y):
            resp = False
        my_perc.learn(x, y, resp)
        
    print(my_perc.getWeight1(), my_perc.getWeight2())
    
    ans_true_listX = []
    ans_true_listY = []
    ans_false_listX = []
    ans_false_listY = []
    
    for i in range(1000):
        x = random.randint(-100,100)
        y = random.randint(-100,100)
        if(my_perc.getOutput(x,y)):
            ans_true_listX.append(x)
            ans_true_listY.append(y)
        else:
            ans_false_listX.append(x)
            ans_false_listY.append(y)
            
        


    plt.plot(ans_true_listX, ans_true_listY, "bo")
    plt.plot(ans_false_listX, ans_false_listY, "ro")
    plt.plot([-100, 100], [(-2*-100)+3, (-2*100)+3], 'k')
    plt.show()
    
    
    
    '''
    Ultimos resultados para los pesos
    6.73 3.379 -> 1000 learnings
    2.27 1.54  -> 100 learnings
    3.13 1.359
    3.129 1.609      
    '''



if __name__ == '__main__':
    main()